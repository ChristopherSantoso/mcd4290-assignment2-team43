if (localStorage !== undefined){
    console.log("there is local storage")
    if(localStorage.getItem("createdShips") !== null){
        var dummyShips= JSON.parse(localStorage.getItem("createdShips"))
        myShipList.retriveFromLocalStorage(dummyShips)
        console.log(dummyShips)
        console.log(myShipList)
    }
}else{
    console.log("no local storage")
}

function createShip(){
    let name=document.getElementById("nameOfTheShip").value
    let range=Number(document.getElementById("travelRange").value)
    let cost=Number(document.getElementById("costPerKM").value)
    let maxSpeed = Number(document.getElementById("maximumSpeed").value)
    let desc= document.getElementById("description").value
    
    myShipList.addShip(new ship(name, maxSpeed, range, desc, cost))
    console.log(myShipList.ships)
    
    localStorage.setItem("createdShips",JSON.stringify(myShipList))
    alert("Ship "+name+" is created !")
    window.location.href = 'viewShips.html';
}