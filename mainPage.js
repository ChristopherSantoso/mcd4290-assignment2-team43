if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdRoutes") !== null){ //Checking if createdPorts is excistant
        var dummyRoutes= JSON.parse(localStorage.getItem("createdRoutes"))
        myRouteList.retriveFromLocalStorage(dummyRoutes)
        console.log(dummyRoutes)
        console.log(myRouteList)
    }
    
}else{
    console.log("no local storage")
}
//=======================Resource========================
//=======================================================




for(var i=0; i<myRouteList.routes.length; i++){
    //On this loop, make the buttons that display on the page
    //split it into :
    //-Current Trips
    //-Upcoming Trips
    //-Past Trips
    
    let thisRoute=myRouteList.routes[i]
    console.log(thisRoute)
    let outputArea=""
    outputArea+="<tr>" 
    outputArea+="<td onmousedown=\"showRoute("+i+")\" class=\"full-width mdl-data-table__cell--non-numeric\" >"+ thisRoute.name
    outputArea+="<div class=\"subtitle\">"+ "Total distance is " + thisRoute.distance +"kms" + " and will take " + thisRoute.time +" day(s) using " + thisRoute.ship +"."
    outputArea+="</div></td></tr>"
    
    var nowDate= new Date().toISOString().slice(0,10)//get today's date

    //These statements should make buttons that contains the information of each routes
    //the placement is based on the date.
    if(myRouteList.routes[i].startDate === nowDate){ //within 24hours
        document.getElementById("currentTrips").innerHTML+=outputArea
    }else if(myRouteList.routes[i].startDate > nowDate ){//in the future
        //Put inside Upcoming Trips
        document.getElementById("upcomingTrips").innerHTML+=outputArea
        
    }else if(myRouteList.routes[i].startDate < nowDate){//in past
        //Put inside Past Trips
        document.getElementById("pastTrips").innerHTML+=outputArea
    }

}
            
function showRoute(ID){
    localStorage.setItem("ID",ID)//will set which route is to be shown
    window.location.href = 'viewRoute.html'//redirecting to viewRoute(Full map view)
}