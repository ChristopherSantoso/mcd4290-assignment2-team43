if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdPorts") !== null){ //Checking if createdPorts is excistant
        var dummyPorts= JSON.parse(localStorage.getItem("createdPorts"))
        myPortList.retriveFromLocalStorage(dummyPorts)
        console.log(dummyPorts)
        console.log(myPortList)
    }
    
}else{
    console.log("no local storage")
}
//End of Parsing
//==================================================================



//The Callback function that will run when JSON is being requested
function makePort(openCageObject) {
    let name = document.getElementById("portName").value
    let country=document.getElementById("portCountry").value
    let type=document.getElementById("portType").value
    let size = document.getElementById("portSize").value
    
    //Reading the JSON object
    portLL = openCageObject.results[0].geometry;
    let latitude = portLL.lat
    let longitude= portLL.lng
    console.log(latitude +","+longitude)
    
    myPortList.addPorts(new port(name, country, type, size, latitude, longitude))
    console.log(myPortList.ports)
    
    localStorage.setItem("createdPorts",JSON.stringify(myPortList))
    alert("Port "+name+" is created !")
    window.location.href = 'viewPorts.html'; 

}

//this function is only to call JSON, making the port will be on the callback function
function createPortButton(){ 
    let location= document.getElementById("portLocation").value
    //=================================================================
    //Accessing openCage.API
    let data = {
        q: location,
        key: "69d98f46dcf84b7499fe8739c6dca677",
        callback: "makePort" 
    };
    
    //jsonRequest call the opneCage.API
    jsonpRequest("https://api.opencagedata.com/geocode/v1/json", data); //the main function is in shared.js
}
