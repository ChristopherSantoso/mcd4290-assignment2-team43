        //=================================================================
        //Parsing myRouteList
        //=================================================================
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdRoutes") !== null){ //Checking if createdPorts is excistant
        var dummyRoutes= JSON.parse(localStorage.getItem("createdRoutes"))
        myRouteList.retriveFromLocalStorage(dummyRoutes)
        console.log(dummyRoutes)
        console.log(myRouteList)
    }
    
}else{
    console.log("no local storage")
}

        //=================================================================
        //Parsing myShipList & defaultShip
        //=================================================================
 
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdShips") !== null){
        var dummyShips= JSON.parse(localStorage.getItem("createdShips"))
        myShipList.retriveFromLocalStorage(dummyShips)
    }
}else{
    console.log("no local storage")
}

let defShip={}
if(localStorage.getItem("defaultShips")!==null){
    //console.log(defaultShip)
    let APIShip=JSON.parse(localStorage.getItem("defaultShips"))
    console.log(APIShip)
    defShip=APIShip
    console.log(defShip)
}else{
    alert("Default Ship has not been created, please go back to VIEW ALL SHIP page")
}

var allShip = []
//Filling the allShip

for(let i=0; i<defShip.ships.length;i++){
    allShip.push(defShip.ships[i])
}
for (let i=0; i< myShipList.ships.length; i++){
    allShip.push(myShipList.ships[i]) 
}
console.log(allShip)

        //================================================================== 
        // Parsing local storage for Ports
        //================================================================== 
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdPorts") !== null){ //Checking if createdPorts is excistant
        var dummyPorts= JSON.parse(localStorage.getItem("createdPorts"))
        myPortList.retriveFromLocalStorage(dummyPorts)
        console.log("myPortList is available")
    }
    
}else{
    console.log("no local storage")
}

let defPort
if(localStorage.getItem("defaultPorts")!==null){
    defPort=JSON.parse(localStorage.getItem("defaultPorts"))
}else{
    alert("Default Ship has not been created, please go back to VIEW ALL PORT page")
}

var allPort = []//AN ARRAY TO CONTAIN ALL PORTS AVAILABLE
defaultPort(defPort)
//=======================END OF PARSING============================


//=================================================================
//Date
//=================================================================
let routeDate, dateInput=false
function getTheDate(e){
    routeDate = e.value
    console.log(typeof routeDate)
    dateInput=true//CONDITIONAL statement for saveRoute (line ~570)
}

//==================================================================  
// Creating the Port Variable for drop down list
//==================================================================
function defaultPort(APIPorts){
    //This is a callback function 
    //By the time this function finishes, allPort will be filled with created ports and ports.API
   var tempPortList = APIPorts.ports
   allPort = tempPortList
    for (let i=0; i< myPortList.ports.length; i++){
        allPort.push(myPortList.ports[i]) 
    }//allPort is filled
    
    
    //this will fill the DROP DOWN LIST CONTENT
    //with COUNTRY LIST coming from port list
    //By the time this function finishes, countryArray will be FILLED
    var countryArray = []
    for (let i = 0; i < allPort.length; i++){
        if (countryArray.includes(allPort[i].country) === false){
            countryArray.push(allPort[i].country)
        }
    }
    countryArray.sort()
    
    //This iterates through country Array and pass the country name
    //to the country drop down list
    for (let i=0; i<countryArray.length;i++){
        let myOptionRef1=document.createElement("option")
        let startCountryRef = document.getElementById('startCountry')
        myOptionRef1.setAttribute("value",countryArray[i])
        myOptionRef1.innerHTML=countryArray[i]
        startCountryRef.appendChild(myOptionRef1)
        
        let myOptionRef2=document.createElement("option")
        let endCountryRef = document.getElementById('endCountry')
        myOptionRef2.setAttribute("value",countryArray[i])
        myOptionRef2.innerHTML=countryArray[i]
        endCountryRef.appendChild(myOptionRef2)
    }
}

function portDDL(SelectCountry){
    //This function will iterate through allPort
    //then store allports that has the same country as the selected country
    
    let countryPorts=[]
    for (let i=0; i< allPort.length; i++){
        if (allPort[i].country===SelectCountry){
            countryPorts.push(allPort[i].name)
        }
    } 
    return countryPorts //array ports' name in a selected country
}


//==================================================================
// Create Port drop downlist
//==================================================================
let startCountry, endCountry, check1, check2
var startCountryPort = [], endCountryPort=[]


let DDLDecision ={
    //this object conscist of METHOD for dropDownList Decision 
    //on choosing country and corresponding port
    checkStartCountry(){
        startCountry = document.getElementById('country1').value
        console.log(startCountry)
        check1 =true
        startCountryPort = portDDL(startCountry) //this will fill the startCountryPort
        choosePort() 
        //this may not run the choosePort() fully because it is being rejected by the first 
        //conditional statement, but will run once destination country is selected.
    },
    
    checkEndCountry(){
        endCountry = document.getElementById('country2').value
        console.log(endCountry)
        check2 =true
        endCountryPort = portDDL(endCountry)
        choosePort()
    }
    
}

let leftRef = document.getElementById('left')
let rightRef = document.getElementById('right')
let output = "", output1 = "", availability
function choosePort(){
    //This function is to :
    //  1. MAKE Drop down list element
    //  2. UPDATE the drop down list content with the correct ship
    
    if(check1 && check2){
        //Check CONDITION if both country is already entered
        if(availability!==1){ 
            //This if statement is built to ONLY RUN ONCE
            output += "<div id=\"left3\"class=\"mdl-textfield mdl-js-textfield\">"
            output += "<input class=\"mdl-textfield__input\" list=\"startPort\" id=\"port1\" name=\"port-choice\" />"
            output += "<datalist id=\"startPort\" name=\"port\">"
            output += "<option value=\"\">"
            output += "</datalist>"
            output += "<label class=\"mdl-textfield__label\" for=\"port1\">Port</label>"
            output += "</div>"
            leftRef.innerHTML += output
                        
            
            output1 += "<div class=\"mdl-textfield mdl-js-textfield\">"
            output1 += "<input class=\"mdl-textfield__input\" list=\"endPort\" id=\"port2\" name=\"port-choice\" />"
            output1 += "<datalist id=\"endPort\" name=\"port\">"
            output1 += "<option value=\"\">"
            output1 += "</datalist>"
            output1 += "<label class=\"mdl-textfield__label\" for=\"port2\">Port</label>"
            output1 += "</div>"
            rightRef.innerHTML += output1
            
            availability=1 //Set the condition so it doesnt make another on
            
        }
        //REMOVING the previous OPTIONS on DDL  
        let prevOptionRef1 = document.getElementById('startPort')//This is startPort drop down list
        prevOptionRef1.innerHTML=''
        let prevOptionRef2= document.getElementById('endPort')//This is endPort drop down list
        prevOptionRef2.innerHTML=''
        
        
        //For loop for making DDL to startCountryPort DOWN HERE
        for (let i=0; i<startCountryPort.length;i++){ //This function should iterate through startCountryPort
            let startPortRef = document.getElementById('startPort')
            let myOptionRef3=document.createElement("option")
            myOptionRef3.setAttribute("value",startCountryPort[i])
            myOptionRef3.innerHTML=startCountryPort[i]
            startPortRef.appendChild(myOptionRef3)
        }
        
        for (let i=0; i<endCountryPort.length;i++){ //This function should iterate through endCountryPort
            let myOptionRef4 = document.createElement("option")
            let endPortRef = document.getElementById('endPort')
            myOptionRef4.setAttribute("value",endCountryPort[i])
            myOptionRef4.innerHTML=endCountryPort[i]
            endPortRef.appendChild(myOptionRef4)
        }
    }
}

//=================================================================
//PUT ANOTHER JS here
//=================================================================


//==================================================================
//Function to return distance
//==================================================================
function distance(loc1,loc2){ 
    //takes 2 ARRAYS containing longitude and latitude values
    //this FUNCTION will RETURN the DISTANCE between 2 points
    
    let lon1=loc1[0], lon2=loc2[0]
    let lat1=loc1[1], lat2=loc2[1]
    
    let R = 6371e3; // metres
    let t1 = lat1 * Math.PI/180; // φ, λ in radians
    let t2 = lat2 * Math.PI/180;
    let dt = (lat2-lat1) * Math.PI/180;
    let dl = (lon2-lon1) * Math.PI/180;

    let a = Math.sin(dt/2) * Math.sin(dt/2) +
          Math.cos(t1) * Math.cos(t2) *
          Math.sin(dl/2) * Math.sin(dl/2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

    let d = R * c /1000; // in km
    return d.toFixed(0)
}

function totalDistance(coorArray){
    //this FUNCTION will RETURN the TOTAL distance
    //this will run the function DISTANCE() to get total distance
    //between several points
    
    var Q=0 //this will store the distance 
    for(var i=0; i<coorArray.length-1;i++){
        Q+=Number(distance(coorArray[i],coorArray[i+1]))
    }
    return Q
}

//=================================================================
//Function to find port
//=================================================================
function findObject(Name,objectArray){
    for (let i =0;i<objectArray.length;i++){
        if (objectArray[i].name===Name){
            return objectArray[i]
        }
    }
}

//==================================================================
//PUT the JS for map.API down here
//==================================================================

//INITIAL VARIABLE NEEDED(Global)
let mapBodyRef= document.getElementById("mapBody")
let routeDistRef= document.getElementById("routeDistanceHTML")
let routeCostRef = document.getElementById("routeCostHTML")
let routeTimeRef= document.getElementById("routeTimeHTML")
mapboxgl.accessToken = 'pk.eyJ1IjoidGVsb21hbmlzIiwiYSI6ImNrY3lnNTc5MzA5c2Yyc3RpeXp2enYxOHEifQ.WxBUpLwgKiIk4FV1ZYv1WA';

let mapPortStart= [] 
let mapPortEnd=  [] 
let routeCrdt= []
let routeDistance
let map,routeData={}
let startPort, endPort
let chooseShipTrigger=0

function showMap(){
    //the function will run when CREATE_A_ROUTE button is clicked
    
    //taking the value of selected ports
    let startPortName= document.getElementById("port1").value
    let endPortName= document.getElementById("port2").value
    console.log(startPortName+" to "+endPortName)
    
    //finding the CORRESPONDING port object
    startPort=findObject(startPortName,allPort)
    endPort=findObject(endPortName,allPort)
        
    mapPortStart=[startPort.lng,startPort.lat]
    mapPortEnd=[endPort.lng,endPort.lat]
    routeCrdt=[mapPortStart,mapPortEnd]//is a waypointList containing ports lngLat from start to finish
    console.log(routeCrdt)
    
    //map center is destinated to be in the middle of 2 ports.
    let mapCenter= [(mapPortEnd[0]+mapPortStart[0])/2,(mapPortEnd[1]+mapPortStart[1])/2]
    
    //=================================================================
    //CRUCIAL PART of showing the Map
    //=================================================================
    map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom:2 ,
        center: mapCenter
    });


    //variable for making ROUTE LINES  
    routeData ={
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates': routeCrdt
        }      
    }
    console.log(routeData)
    

    
    map.on('load',function(){
    
        //this will RUN when map is LOADED
        //below are the essential part to make ROUTE LINES
        map.addSource('route1', {
            'type': 'geojson',
            'data': routeData
        })
        console.log(map.getSource('route1'))
    
        map.addLayer({ //making LAYER when the LINES are going to be
            'id': 'route',
            'type': 'line',
            'source': 'route1',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        })
    
        //Make MARKER for start and end point
        new mapboxgl.Marker({color: '#FF0000'}) //START_POINT
            .setLngLat(mapPortStart)
            .addTo(map);
        new mapboxgl.Marker({color: '#008000'}) //END_POINT
            .setLngLat(mapPortEnd)
            .addTo(map);

        //OUTPUTING value of INITIAL DISTANCE (start to end)
        routeDistance= totalDistance(routeCrdt)
        routeDistRef.innerHTML=routeDistance+"kms"
        
        //showing the map and div
        document.getElementById("show").hidden = false
    })

    map.on('click', function(e) { 
        //This is the function that will run map is clicked to add waypoint
        //below, there will be nested IF STATEMENT that wil decide whether the waypoint is eligible or not
        //Requirement : waypoint should be 100kms away from one another

        let latitude= e.lngLat.lat
        let longitude= e.lngLat.lng
        let newPoint=[longitude,latitude]
        let insert=true //default CONDITION to make the marker

        for(var i =0;i<routeCrdt.length;i++){//STATEMENT decides whether new point can be added or not
            let bigger100 = distance(newPoint,routeCrdt[i])
            if (bigger100 <100){
                insert=false //only accept that is farther than 100km
                break
            }
        }

        if(insert){ //conditional statement line ~385
            //FUNCTION that will run if the new point is farther than 100kms from one another
            routeCrdt.splice(routeCrdt.length-1, 0, newPoint)
            routeData.geometry.coordinates=routeCrdt
            console.log(routeData.geometry.coordinates)

            routeDistance= totalDistance(routeCrdt)
            routeDistRef.innerHTML=routeDistance+"kms"

            map.getSource('route1').setData(routeData)
            map.removeLayer('route')
            //removing than readding layer (refresh the layer)
            map.addLayer({
                'id': 'route',
                'type': 'line',
                'source': 'route1',
                'layout': {
                    'line-join': 'round',
                    'line-cap': 'round'
                },
                'paint': {
                    'line-color': '#888',
                    'line-width': 8
                }
            })

            new mapboxgl.Marker({color: '#fed8b1'})
            .setLngLat(newPoint)
            .addTo(map);

        }else{
            alert("Marker is too close ! It should be more than 100km away from one another")
        }
        
        if (chooseShipTrigger===1){
            chooseShip(2)//call ing a chooseShip() with source2 (Map onclick)
            changeShip()//will make it recalculate the "cost" and other information
        }
    })
}

//=================================================================
//Choose Ship
//=================================================================
//Below are the code for Choose Ship button
function chooseShip(Source){
    Source=Number(Source)
    //Source1 is from button
    //Source2 is from map.onlick after the change 
    let travelRange= routeDistance
    //at this moment you will have an array that contains every single ships available
    //whether from the API or from the user created ones

    //make a loop that will iterates through every single elements LOOKING FOR ships that
    //has distance more than the given distance
    let readyShip = []
    for (let i=0; i< allShip.length;i++){
        if (allShip[i].range >= travelRange && allShip[i].status==="available"){
            readyShip.push(allShip[i].name)
        }
    }

    //sort the readyShip Array
    readyShip.sort()

    //make the DDL
    
    if(Source===1){//only runs when the it s from the BUTTON
        let chooseShipDDLRef = document.getElementById('chooseShipDDL')
        chooseShipDDLRef.innerHTML=""
        let output4 = ""
        output4 += "<div class=\"mdl-textfield mdl-js-textfield\">"
        output4 += "<input class=\"mdl-textfield__input\" list=\"routeShip\" id=\"ship1\" name=\"ship-choice\" onchange=\"changeShip()\"/>"//onchange havent done yet
        output4 += "<datalist id=\"routeShip\" name=\"ship\">"
        output4 += "<option value=\"\">"
        output4 += "</datalist>"
        output4 += "<label class=\"mdl-textfield__label\" for=\"ship1\">Choose a ship</label>"
        output4 += "</div>"
        chooseShipDDLRef.innerHTML += output4
    }
    
    //fill the DDL 
    if(Source===2 && chooseShipTrigger===1){
        let previousShipDDLRef= document.getElementById('routeShip')
        previousShipDDLRef.innerHTML=''
    }
    
    for (let i=0; i<readyShip.length;i++){ //This function should iterate through startCountryPort
        let routeShipRef = document.getElementById('routeShip')
        let myOptionRef4=document.createElement("option")
        myOptionRef4.setAttribute("value",readyShip[i])
        myOptionRef4.innerHTML=readyShip[i]
        routeShipRef.appendChild(myOptionRef4)
    }
    
    if (Source===1){//only runs when it s from the BUTTON
        chooseShipTrigger=1
    }
}

//==========================================
// function changeShip()
//==========================================
let chosenShip, tripCost,tripTime
let shipInput=false, costInput= false, timeInput=false

function changeShip(){
    //this function RUNS everytime the SHIP IS CHANGED
    // and when NEW WAYPOINT is ADDED
    let shipName=document.getElementById("ship1").value
    console.log(shipName)
    if(shipName!== ''){// only runs if there is a ship selected
        chosenShip=findObject(shipName,allShip)
        console.log(chosenShip)
        
        //This statement is to calculate the cost & total distance
        if (chosenShip.range>=routeDistance){
            tripCost=Number(chosenShip.cost) * Number(routeDistance)
            let temp=Number(chosenShip.maxSpeed) * 1.852
            tripTime=Number(routeDistance)/(temp*24)
            tripTime=Math.ceil(tripTime)
            //console.log(cost +","+time)
            routeCostRef.innerHTML="$"+tripCost.toFixed(2)
            routeTimeRef.innerHTML=tripTime+" days"
            shipInput=true
            timeInput=true
            costInput=true
        }else{
            routeCostRef.innerHTML="Ship uncapable"
            routeTimeRef.innerHTML="Ship uncapable"
            shipInput=false
            timeInput=false
            costInput=false  
        }
    }
}

//==========================================
// function SAVE A ROUTE
//==========================================
function saveRoute(){
    //this function will check whether every component has been filled
    //before saving it
    if(dateInput){
        var startDate= routeDate
    }
    
    if(shipInput){
        var ship= chosenShip.name
        console.log(chosenShip)
    }
    
    if(endPort!== undefined){
        var destinationPort= endPort.name
        var endPInput= true
    }
    
    if (startPort!== undefined){
        var sourcePort= startPort.name
        var startPInput=true
    }
    
    if (routeDistance!== undefined){
        var distance= routeDistance
        var disInput=true
    }
    
    if (routeCrdt!== []){
        var wayPointList= routeCrdt
        var wayInput=true
    }
    
    if(costInput){
        var cost= tripCost.toFixed(2)
    }
    
    if(timeInput){
        var time=tripTime
    }
    
    if(dateInput && endPInput && startPInput && disInput && wayInput && costInput && timeInput & shipInput){
        let name = startPort.name +" to "+ endPort.name
        myRouteList.addRoute(new route(name,ship,sourcePort,destinationPort,distance,time, cost, startDate, wayPointList))
        console.log(myRouteList)
        
        //================EDITING SHIP STATUS======================
        //this will get the index of the selected ship in allShips
        console.log(allShip)
        let shipIndex=allShip.findIndex(obj => obj.name===chosenShip.name)
        console.log(shipIndex)
        
               
        if(shipIndex<defShip.ships.length){ 
            //if YES, then this ship is from defaultShips
            defShip.ships[shipIndex].status="unavailable"
            localStorage.setItem("defaultShips",JSON.stringify(defShip))
        }else{
            //if NO, then this is a created ship.
            let MSLIndex=shipIndex-defShip.ships.length
            console.log(MSLIndex)
            myShipList.ships[MSLIndex].status="unavailable"
            localStorage.setItem("createdShips",JSON.stringify(myShipList))
        }
        console.log(defShip)
        
        localStorage.setItem('createdRoutes',JSON.stringify(myRouteList))
        alert("Route "+name+" has been created !")
        window.location.href = 'index.html'
        
        
    }else{
        alert("Something have not been filled !")
    }
}
    
       
