class ship{
    constructor (name, maxSpeed, range, desc, cost){
        this._name = name
        this._maxSpeed = maxSpeed
        this._range=range
        this._desc=desc
        this._cost=cost
        this._status="available"
    }
    
    get name(){
        return this._name
    }
    
    set name(Name){
        this._name=Name
    }
    
    get maxSpeed(){
        return this._maxSpeed
    }
    
    set maxSpeed(knots){
        if (typeof knots === "number"){
            this._maxSpeed=knots
        }else{
            this._maxSpeed=Number(knots)
        }
    }
    
    get range(){
        return this._range
    }
    
    set range(km){
        if (typeof km === "number"){
            this._range=km
        }else{
            this._range=Number(km)
        }
    }
    
    get desc(){
        return this._desc
    }
    
    set desc(description){
        this._desc=description
    }
    
    get cost(){
        return this._cost
    }
    
    set cost(money){
        if (typeof money === "number"){
            this._cost=money
        }else{
            this._cost=Number(money)
        }
    }
    
    get status(){
        return this._status
    }
    
    set status(stat){
            this._status=stat
    }
    
    initialiseFromShipPDO(iShip){
        // Initialise the instance via the mutator methods from the PDO object.
        this.name = iShip._name
        this.maxSpeed= iShip._maxSpeed
        this.range= iShip._range
        this.desc=iShip._desc
        this.cost=iShip._cost
        this.status=iShip._status
    }
}

class port{
    constructor (name, country, type, size, latitude, longitude){
        this._name=name
        this._country=country
        this._type=type
        this._size=size
        this._lat=latitude
        this._lng=longitude 
    }
    get name(){
        return this._name
    }
    set name(NAMe){
        this._name=NAMe
    }
    
    get country(){
        return this._country
    }
    set country(Coun){
        this._country= Coun
    }
    get type(){
        return this._type
    }
    set type(Type){
        this._type= Type
    }
    
    get size(){
        return this._size
    }
    set size(Size){
        this._size = Size
    }
    
    get lat(){
        return this._lat
    }
    
    set lat(Latitude){
        this._lat=Latitude
    }
    
    get lng(){
        return this._lng
    }
    set lng(Longitude){
        this._lng=Longitude
        /*
        if (typeof Longitude === "number"){
            this._longitude=Longitude
        }else{
            this._longitude=Number(Longitude)
        }
        */
    }
     initialiseFromPortPDO(iPort){
        console.log(iPort)
        // Initialise the instance via the mutator methods from the PDO object.
        this.name = iPort._name
        this.country=iPort._country
        this.type=iPort._type
        this.size=iPort._size
        this.lat=iPort._lat
        this.lng=iPort._lng 
       
    }
}

class route{
    constructor (name,ship,sourcePort,destinationPort,distance,time, cost, startDate, wayPointList){
        this._name=name
        this._ship=ship
        this._sourcePort=sourcePort
        this._destinationPort=destinationPort
        this._distance=distance
        this._time=time
        this._cost=cost
        this._startDate=startDate
        this._wayPointList=wayPointList
    }
    
    get name(){
        return this._name
    }
    
    set name(Name){
        this._name=Name
    }
    
    get ship(){
        return this._ship
    }
    
    set ship(Ship){
        this._ship=Ship
    }
    
    get sourcePort(){
        return this._sourcePort
    }
    
    set sourcePort(Source){
        this._sourcePort=Source
    }
    
    get destinationPort(){
        return this._destinationPort
    }
    
    set destinationPort(Des){
        this._destinationPort=Des
    }
    
    get distance(){
        return this._distance
    }
    
    set distance(Distance){
        this._distance=Distance
    }
    
    get time(){
        return this._time
    }
    
    set time(Time){
        this._time=Time
    }
    
    get cost(){
        return this._cost
    }
    
    set cost(Cost){
        this._cost=Cost
    }
    
    get startDate(){
        return this._startDate
    }
    
    set startDate(Date){
        this._startDate=Date
    }
    
    get wayPointList(){
        return this._wayPointList
    }
    
    set wayPointList(Point){
        this._wayPointList=Point
    }
    
    initialiseFromRoutePDO(iRoute){
        console.log(iRoute)
        // Initialise the instance via the mutator methods from the PDO object.
        this.name = iRoute._name
        this.startDate=iRoute._startDate
        this.sourcePort=iRoute._sourcePort
        this.destinationPort=iRoute._destinationPort
        this.distance=iRoute._distance
        this.time=iRoute._time 
        this.cost=iRoute._cost
        this.wayPointList=iRoute._wayPointList
        this.ship=iRoute._ship
    }
    
}

class shipList{
    constructor (){
        this._ships=[]
    }
    
    get ships(){
        return this._ships
    }
    
    addShip(Ship){
        this.ships.push(Ship)
    }
    
    retriveFromLocalStorage(dummyShips){
        
        for(var i=0;i<dummyShips._ships.length;i++){
            var iShip=dummyShips._ships[i]
            this.ships[i]=new ship()
            this.ships[i].initialiseFromShipPDO(iShip)
        }
    }
}


class portList{
    constructor(){
        this._ports=[]
    }
    get ports(){
        return this._ports
    }
    
    addPorts(port){
        this.ports.push(port)
    }
    
    retriveFromLocalStorage(dummyPorts){
        console.log(dummyPorts)
        for(var i=0;i<dummyPorts._ports.length;i++){
            var iPort=dummyPorts._ports[i]
            console.log(iPort)
            this.ports[i]=new port()
            this.ports[i].initialiseFromPortPDO(iPort)
            
        }
    }
}

class routeList{
    constructor(){
        this._routes=[]
    }
    
    get routes(){
        return this._routes
    }
    
    addRoute(route){
        this.routes.push(route)
    }
    
    retriveFromLocalStorage(dummyRoutes){
        console.log(dummyRoutes)
        for(var i=0;i<dummyRoutes._routes.length;i++){
            var iRoutes=dummyRoutes._routes[i]
            console.log(iRoutes)
            this.routes[i]=new route()
            this.routes[i].initialiseFromRoutePDO(iRoutes)
        }
    }
}
//================================================================================
//JSON FUNCTION
//===============================================================================
function jsonpRequest(url, data) //Data is an object concisting JSON informations
    {
    // Build URL parameters from data object.
        let params = "";
    // For each key in data object...
        for (let key in data)
        {
            if (data.hasOwnProperty(key))
            {
                if (params.length == 0)
                {
                // First parameter starts with '?'
                    params += "?";
                }
                else
                {
                // Subsequent parameter separated by '&'
                    params += "&";
                }

                let encodedKey = encodeURIComponent(key);
                let encodedValue = encodeURIComponent(data[key]);

                params += encodedKey + "=" + encodedValue;
           }  
        }
        let script = document.createElement('script');
        script.src = url + params;
        document.body.appendChild(script);
    }

// declaring new global variable
let myShipList = new shipList
let myPortList = new portList
let myRouteList= new routeList