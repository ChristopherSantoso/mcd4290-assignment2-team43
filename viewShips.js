//=================================================================
//Parsing from local storage
//=================================================================
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdShips") !== null){
        var dummyShips= JSON.parse(localStorage.getItem("createdShips"))
        myShipList.retriveFromLocalStorage(dummyShips)
        console.log(dummyShips)
        console.log(myShipList)
    }
}else{
    console.log("no local storage")
}

//=================================================================
// Creating Cards
//=================================================================
for (let i = 0; i<myShipList.ships.length; i++){
    //Creating Element inside body1
    let body1Ref = document.getElementsByTagName("body1")[0]
    
    let gridDivRef=document.createElement("div")
    gridDivRef.setAttribute("class","mdl-cell mdl-cell--4-col")
    gridDivRef.setAttribute("id","shipCard"+i)
    
    let newDiv = document.createElement("div")
    newDiv.setAttribute("class", "demo-card-square mdl-card mdl-shadow--2dp")
    
    let newH3= document.createElement("h3")
    newH3.setAttribute("id","shipTitleOut" + i)
    newH3.setAttribute("class","mdl-card__title mdl-card--expand")
    
    let newH6 = document.createElement("h6")
    newH6.setAttribute("id","shipDescOut" + i)
    newH6.setAttribute("class","mdl-card__title mdl-card--expand")
    
    let newDiv2 = document.createElement("div")
    newDiv2.setAttribute("class", "mdl-card__actions mdl-card--border")
    
    let newBut = document.createElement("button")
    newBut.setAttribute("class", "mdl-button mdl-js-button mdl-button--icon")
    
    let newI= document.createElement("i")
    newI.setAttribute("class","material-icons")
    newI.innerHTML="delete"
    newI.setAttribute("onclick","Delete("+i+")")

    //Appending child to each corresponding element
    newDiv.appendChild(newH3)
    newDiv.appendChild(newH6)
    newBut.appendChild(newI)
    newDiv2.appendChild(newBut)
    newDiv.appendChild(newDiv2)
    gridDivRef.appendChild(newDiv)
    body1Ref.appendChild(gridDivRef)
    
    // Output
    let titleRef= document.getElementById("shipTitleOut" + i)
    let descRef= document.getElementById("shipDescOut" + i)
    titleRef.innerHTML = myShipList.ships[i].name + "<br/>" + " @$" + myShipList.ships[i].cost + " per km"
    descRef.innerHTML = "Max Speed: " + myShipList.ships[i].maxSpeed + "<br/>" + "Range: " + myShipList.ships[i].range + " in km" + "<br/>" + "Description: " + myShipList.ships[i].desc + "<br/>" + "Status: " + myShipList.ships[i].status
}

//=================================================================
// Delete function
//=================================================================
function Delete(ID){
    var confirmation= confirm("Are you sure you want to delete "+myShipList.ships[ID].name+" ship ?")
    if(confirmation){
        //Removing the HTML element
        let selectedDiv= document.getElementById("shipCard"+ID)
        console.log(selectedDiv)
        selectedDiv.parentNode.removeChild(selectedDiv)
        //Removing Selected Ship from the MyShipList.ships array
        myShipList.ships.splice(ID,1)
        console.log(myShipList)

        //Updating the local storage
        localStorage.setItem("createdShips",JSON.stringify(myShipList))
    }
}

//=================================================================
// Make API request || parsing defaultShips from local Storage
//=================================================================
//This statement will check whether it needs to get from API or 
//parse from local storage (the edited one ==> to unavailable)
if(localStorage.getItem("defaultShips")===null){
    var url = "https://eng1003.monash/api/v1/ships/?callback=defaultShip"
    var script = document.createElement('script')
    script.src = url
    document.body.appendChild(script)
}else{
    //This will pull the default ship list that has been edited from 
    //addRoute (available || unavailable)
    let shipsArray=JSON.parse(localStorage.getItem("defaultShips")).ships
    makeShipCard(shipsArray)
    
}

//=================================================================
// Ships API
//=================================================================
function defaultShip(APIShips){
    let shipsArray= APIShips.ships
    localStorage.setItem("defaultShips",JSON.stringify(APIShips))
    
    // create default card
    makeShipCard(shipsArray)
}

//=================================================================
// Make ship card
//=================================================================

function makeShipCard(shipsArray){
    //This function is only to display cards containing API ships information
    
    for (let i = 0; i < shipsArray.length; i++){ 
        //Creating Element inside body2
        let body2Ref = document.getElementsByTagName("body2")[0]
    
        let gridDivRef=document.createElement("div")
        gridDivRef.setAttribute("class","mdl-cell mdl-cell--3-col")
        gridDivRef.setAttribute("id","shipCard"+i)
    
        let newDiv = document.createElement("div")
        newDiv.setAttribute("class", "demo-card-square mdl-card mdl-shadow--2dp")
    
        let newH3= document.createElement("h3")
        newH3.setAttribute("id","defShipTitleOut" + i)
        newH3.setAttribute("class","mdl-card__title mdl-card--expand")
    
        let newH6 = document.createElement("h6")
        newH6.setAttribute("id","defShipDescOut" + i)
        newH6.setAttribute("class","mdl-card__title mdl-card--expand")
    
        //Appending child to each corresponding element
        newDiv.appendChild(newH3)
        newDiv.appendChild(newH6)
        gridDivRef.appendChild(newDiv)
        body2Ref.appendChild(gridDivRef)
    
    // Output
    let defShipTitleRef = document.getElementById('defShipTitleOut' + i)
    let defShipDescRef = document.getElementById('defShipDescOut' + i)
    
        defShipTitleRef.innerHTML = shipsArray[i].name + "<br/>" + " @$" + shipsArray[i].cost + " per km"
        defShipDescRef.innerHTML = "Max Speed: " + shipsArray[i].maxSpeed + "<br/>" + "Range: " + shipsArray[i].range + " in km" + "<br/>" + "Description: " + shipsArray[i].desc + "<br/>" + "Status: " + shipsArray[i].status
    }
}