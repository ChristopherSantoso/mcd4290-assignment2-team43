//=======================================================
//Change the value of these variables
//=======================================================
let mapPortStart= []
let mapPortEnd= []

//=======================================================
// HTML Variable
//=======================================================
let shipRef = document.getElementById('ship')
let distanceRef = document.getElementById('distance')
let costRef = document.getElementById('cost')
let dateRef = document.getElementById('date')
let timeRef = document.getElementById('time')
let ID

//=======================================================
// Accessing for Map
//=======================================================
mapboxgl.accessToken = 'pk.eyJ1IjoidGVsb21hbmlzIiwiYSI6ImNrY3lnNTc5MzA5c2Yyc3RpeXp2enYxOHEifQ.WxBUpLwgKiIk4FV1ZYv1WA';


//Parsing
let isUserShipCreated=false //conditional statement that tells if there is userShip created.
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdShips") !== null){
        var dummyShips= JSON.parse(localStorage.getItem("createdShips"))
        myShipList.retriveFromLocalStorage(dummyShips)
        isUserShipCreated=true
    }
    
    if(localStorage.getItem("createdRoutes") !== null){ //Checking if createdPorts is excistant
        var dummyRoutes= JSON.parse(localStorage.getItem("createdRoutes"))
        myRouteList.retriveFromLocalStorage(dummyRoutes)
        
        console.log(dummyRoutes)
        console.log(myRouteList)
        ID= Number(localStorage.getItem("ID"))
        console.log(ID)
        showMap(ID)
    }
    
}else{
    console.log("no local storage")
}

let defShip
if(localStorage.getItem("defaultShips")!==null){
    defShip=JSON.parse(localStorage.getItem("defaultShips"))
}else{
    alert("Default Ship has not been created, please go back to VIEW ALL SHIP page")
}
//End of Parsing
//==================================================================


//=======================================================
// Function for Map
//=======================================================

function showMap(ID){
    
    
    //===================================================
    // Variable for Port
    //===================================================
    let point=myRouteList.routes[ID].wayPointList
    mapPortStart=point[0]
    mapPortEnd=point[point.length-1]
    
    //===================================================
    // Variable for map
    //===================================================
    let mapCenter= [(mapPortEnd[0]+mapPortStart[0])/2,(mapPortEnd[1]+mapPortStart[1])/2] //center of the map
    let routeCrdt= point
    let routeDistance =myRouteList.routes[ID].distance
    
    //===================================================
    // This will ONLY show map
    //===================================================
    let map = new mapboxgl.Map({
        container: 'map',
        style: 'mapbox://styles/mapbox/streets-v10',
        zoom:2 ,
        center: mapCenter
    });
    
    //===================================================
    // Variable for making ROUTE LINES 
    //===================================================
    let routeData ={
        'type': 'Feature',
        'properties': {},
        'geometry': {
            'type': 'LineString',
            'coordinates': routeCrdt
        }      
    }
    
    //===================================================
    // Adding Route Line
    //===================================================
    map.on("load",function(){
        map.addSource('route', {
                'type': 'geojson',
                'data': routeData
            })
        map.addLayer({ //making LAYER when the LINES are going to be
            'id': 'route',
            'type': 'line',
            'source': 'route',
            'layout': {
                'line-join': 'round',
                'line-cap': 'round'
            },
            'paint': {
                'line-color': '#888',
                'line-width': 8
            }
        })
    })
    
    //===================================================
    // Pop Up Function
    //===================================================
    function popUp()
        {    
            var popUpStart = new mapboxgl.Marker({color: '#FF0000'})
            .setLngLat(mapPortStart)
            .setPopup(new mapboxgl.Popup().setHTML("<h5> Start Port: </h5>" + "<p>" + myRouteList.routes[ID].sourcePort + "</p>"))
            .addTo(map);
            
             var popUpEnd = new mapboxgl.Marker({color: '#008000'})
            .setLngLat(mapPortEnd)
            .setPopup(new mapboxgl.Popup().setHTML("<h5> End Port: </h5>" + "<p>" + myRouteList.routes[ID].destinationPort + "</p>"))
            .addTo(map);
        }
    //===================================================
    // Adding Marker
    //===================================================
    for (var i=0;i<point.length;i++){
        if(i===0){
            new mapboxgl.Marker({color: '#FF0000'}) //START_POINT
            .setLngLat(mapPortStart)
            .addTo(map);
        }else if(i===point.length-1){
            new mapboxgl.Marker({color: '#008000'}) //END_POINT
            .setLngLat(mapPortEnd)
            .addTo(map);
        }else{
            new mapboxgl.Marker({color: '#fed8b1'}) // MARKER_POINT
            .setLngLat(point[i])
            .addTo(map);
        }
    }
    //===================================================
    // Calling Pop Up Function
    //===================================================
    popUp()
    
    //===================================================
    // Output
    //===================================================
    let price = Number(myRouteList.routes[ID].cost)
    shipRef.innerHTML = myRouteList.routes[ID].ship
    distanceRef.innerHTML = myRouteList.routes[ID].distance + " Kms"
    costRef.innerHTML = "$ " + price.toFixed(2)
    dateRef.innerHTML = myRouteList.routes[ID].startDate
    timeRef.innerHTML = myRouteList.routes[ID].time + " days"
}  

//=======================================================
// Change Date Button
//=======================================================
let changeDateRef = document.getElementById('changeDate')

function changeDate(){
    changeDateRef.innerHTML = ""
    
    let output = ""
    output += "<script src=\"scripts/simple.js\" type=\"text/javascript\"></script>"
    output += "<link rel=\"stylesheet\" href=\"https://code.getmdl.io/1.3.0/material.indigo-pink.min.css\">"
    output += "<div>"
    output += "<div class=\"mdl-textfield mdl-js-textfield mdl-textfield--floating-label has-placeholder\">"
    output += "<input class=\"mdl-textfield__input\" type=\"date\" id=\"startDate\" onchange=\"getTheDate(this)\">"
    output += "<label class=\"mdl-textfield__label\" for=\"startDate\">Date</label>"
    output += "</div>"
    output += "<label id=\"date\"></label>"
    output += "</div>"
    changeDateRef.innerHTML += output
}

//=======================================================
// Date
//=======================================================

function getTheDate(e){
    let routeDate=e.value
    dateRef.innerHTML = routeDate
    myRouteList.routes[ID].startDate=routeDate
    localStorage.setItem("createdRoutes",JSON.stringify(myRouteList))
    
    let nowDate= new Date().toISOString().slice(0,10)//get today's date
    if(routeDate < nowDate){
        //This statement set the used SHIP'S STATUS to AVAILABLE when it is already a PAST TRIP
        let shipName= myRouteList.routes[ID].ship
        let shipIndex 

        //updating the ship availability
        if(isUserShipCreated){
            shipIndex=myShipList.ships.findIndex(obj => obj.name===shipName)
            if (shipIndex>=0){
                console.log("createdShip index= "+ shipIndex)
                myShipList.ships[shipIndex].status="available"
                localStorage.setItem("createdShips",JSON.stringify(myShipList))
            }
        }
        if(shipIndex===undefined || shipIndex<0){
            //shipIndex is undefined from default, meaning that the PREVIOUS statement did not run
            //shipIndex < 0 when it is not found in myShipList.ships
            shipIndex=defShip.ships.findIndex(obj => obj.name===shipName)
            console.log("defShip index= "+ shipIndex)
            defShip.ships[shipIndex].status="available"
            localStorage.setItem("defaultShips",JSON.stringify(defShip))
        }
    }
}

//=======================================================
// Delete button
//=======================================================
function deleteRoute(){
    var confirmation= confirm("Are you sure you want to delete "+myRouteList.routes[ID].name+" route ?")
    if(confirmation){
        let id = ID
        let shipName= myRouteList.routes[ID].ship
        let shipIndex 

        //updating the ship availability
        if(isUserShipCreated){//Check if there is user created ship
            shipIndex=myShipList.ships.findIndex(obj => obj.name===shipName)
            if(shipIndex>=0){
                console.log("createdShip index= "+ shipIndex)
                myShipList.ships[shipIndex].status="available"
                localStorage.setItem("createdShips",JSON.stringify(myShipList))
            }
        }
        if(shipIndex===undefined || shipIndex<0){
            //shipIndex is undefined from default, meaning that the PREVIOUS statement did not run
            //shipIndex < 0 when it is not found in myShipList.ships
            shipIndex=defShip.ships.findIndex(obj => obj.name===shipName)
            console.log("defShip index= "+ shipIndex)
            defShip.ships[shipIndex].status="available"
            localStorage.setItem("defaultShips",JSON.stringify(defShip))
        }

        alert("Route from "+myRouteList.routes[ID].name+" has been deleted. "+shipName+" has been set to available.")
        //Updating the local storage
        myRouteList.routes.splice(ID,1)
        localStorage.setItem("createdRoutes",JSON.stringify(myRouteList))


        //Redirecting to index.HTML
        window.location.href = "index.html"
    }
}

function back(){
   window.location.href = "index.html" 
}