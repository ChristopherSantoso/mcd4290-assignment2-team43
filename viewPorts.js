//=================================================================
//Parsing from local storage
//=================================================================
if (localStorage !== undefined){
    console.log("there is local storage")
    
    if(localStorage.getItem("createdPorts") !== null){ //Checking if createdPorts is excistant
        var dummyPorts= JSON.parse(localStorage.getItem("createdPorts"))
        myPortList.retriveFromLocalStorage(dummyPorts)
        console.log(dummyPorts)
        console.log(myPortList)
    }
    
}else{
    console.log("no local storage")
}

//=================================================================
// Creating Cards for Ports
//=================================================================
for (let i = 0; i<myPortList.ports.length; i++){
    //Creating Element inside body1
    let body1Ref = document.getElementsByTagName("body1")[0]
    
    let gridDivRef=document.createElement("div")
    gridDivRef.setAttribute("class","mdl-cell mdl-cell--4-col")
    gridDivRef.setAttribute("id","portCard"+i)
    
    let newDiv = document.createElement("div")
    newDiv.setAttribute("class", "demo-card-square mdl-card mdl-shadow--2dp")
    
    let newH3= document.createElement("h3")
    newH3.setAttribute("id","portTitleOut" + i)
    newH3.setAttribute("class","mdl-card__title mdl-card--expand")
    
    let newH6 = document.createElement("h6")
    newH6.setAttribute("id","portDescOut" + i)
    newH6.setAttribute("class","mdl-card__title mdl-card--expand")
    
    let newDiv2 = document.createElement("div")
    newDiv2.setAttribute("class", "mdl-card__actions mdl-card--border")
    
    let newBut = document.createElement("button")
    newBut.setAttribute("class", "mdl-button mdl-js-button mdl-button--icon")
    
    let newI= document.createElement("i")
    newI.setAttribute("class","material-icons")
    newI.innerHTML="delete"
    newI.setAttribute("onclick","Delete("+i+")")

    //Appending child to each corresponding element
    newDiv.appendChild(newH3)
    newDiv.appendChild(newH6)
    newBut.appendChild(newI)
    newDiv2.appendChild(newBut)
    newDiv.appendChild(newDiv2)
    gridDivRef.appendChild(newDiv)
    body1Ref.appendChild(gridDivRef)
    

    // Output
    let titleRef= document.getElementById("portTitleOut" + i)
    let descRef= document.getElementById("portDescOut" + i)
    titleRef.innerHTML = "Port name: "+myPortList.ports[i].name 
    descRef.innerHTML =  " type: " + myPortList.ports[i].type + "<br/>" +"port location " + myPortList.ports[i].lng + ","+ myPortList.ports[i].lat + "<br/>" + "Country: " + myPortList.ports[i].country  + "<br/>" + "Size of the port: " + myPortList.ports[i].size 
}

//=================================================================
// Delete Function
//=================================================================
function Delete(ID){
    var confirmation= confirm("Are you sure you want to delete "+myPortList.ports[ID].name+" port ?")
    if(confirmation){
        //Removing the HTML element
        let selectedDiv= document.getElementById("portCard"+ID)
        console.log(selectedDiv)
        selectedDiv.parentNode.removeChild(selectedDiv)
        //Removing Selected Ship from the MyShipList.ships array
        myPortList.ports.splice(ID,1)
        console.log(myPortList)

        //Updating the local storage
        localStorage.setItem("createdPorts",JSON.stringify(myPortList))
    }
}

//=================================================================
// Calling API
//=================================================================
var url = "https://eng1003.monash/api/v1/ports/?callback=defaultPortCard"
var script = document.createElement("script")
script.src = url
document.body.appendChild(script)

//=================================================================
// Port API
//=================================================================
function defaultPortCard(APIPort){
    let ports= APIPort.ports
    localStorage.setItem("defaultPorts",JSON.stringify(APIPort))
    
    for (let i = 0; i<ports.length; i++){
        //myPortList.ports.length
        //Creating Element inside body1
        let body2Ref = document.getElementsByTagName("body2")[0]
    
        let gridDivRef2=document.createElement("div")
        gridDivRef2.setAttribute("class","mdl-cell mdl-cell--3-col")
        gridDivRef2.setAttribute("id","portCard"+i)
    
        let newDiv2 = document.createElement("div")
        newDiv2.setAttribute("class", "demo-card-square mdl-card mdl-shadow--2dp")
    
        let newH3_2= document.createElement("h3")
        newH3_2.setAttribute("id","portTitleOut" + i)
        newH3_2.setAttribute("class","mdl-card__title mdl-card--expand")
    
        let newH6_2 = document.createElement("h6")
        newH6_2.setAttribute("id","portDescOut" + i)
        newH6_2.setAttribute("class","mdl-card__title mdl-card--expand")
    
        //Appending child to each corresponding element
        newDiv2.appendChild(newH3_2)
        newDiv2.appendChild(newH6_2)
        gridDivRef2.appendChild(newDiv2)
        body2Ref.appendChild(gridDivRef2)
        
        // Output
        newH3_2.innerHTML = "Port name: <br/>"+ports[i].name 
        newH6_2.innerHTML =  " type: " + ports[i].type + "<br/>" +"port location " + ports[i].lng + ","+ ports[i].lat + "<br/>" + "Country: " + ports[i].country  + "<br/>" + "Size of the port: " + ports[i].size 
    }
}